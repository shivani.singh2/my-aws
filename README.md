## Infrastructure as a Code
### Note: Continuing from first assignment ......

### Task Description

#### Create the following resources using terraform:

#### 1. Create  ec2-instances based on your  assigned software(cluster or HA(Use Interpolation))

![image](https://user-images.githubusercontent.com/19822562/145523211-58c3d0db-e966-4c5b-800c-ac5eaabeacee.png)

#### 2. Create one bastion host

![image](https://user-images.githubusercontent.com/19822562/145523137-beadc15a-6ab8-4255-99fd-b9830bb692b9.png)

####  Create  Security Groups for  your EC2 instances

#### 1. port 22 of bastion host should only accessible from your public ip.

![image](https://user-images.githubusercontent.com/19822562/145523286-d4edcf7d-c16c-4a1e-a166-589339e62030.png)

#### 2. port 22 of  the private instances should only be accessible from  bastion host

![image](https://user-images.githubusercontent.com/19822562/145523347-2e27ebac-df43-4a38-902d-da5bcad05dcd.png)

  
### Create jenkins pipeline that will create the Infra

#### 1.There should be  approval stage before the infra is created

#### 2.Your job should send notification for status of job in slack.

![image](https://user-images.githubusercontent.com/19822562/145606491-be8c4c44-425a-4c5c-93ea-73533d5c4948.png)

![image](https://user-images.githubusercontent.com/19822562/145600178-89ae3213-aa66-4e50-a7e5-000417f68736.png)

![image](https://user-images.githubusercontent.com/19822562/145600398-af605df0-d856-4e71-9ebd-17ed7339cd70.png)

![image](https://user-images.githubusercontent.com/19822562/145600268-dc2ec828-b6e3-4d25-9194-d2181be39400.png)


### RESULT

![image](https://user-images.githubusercontent.com/19822562/145600962-e62dafd8-2267-45ef-a201-6309887f827c.png)



### Create jenkins pipeline that will destroy the Infra

#### 1. There should be  approval stage before the infra is Destroyed

#### 2. Your job should send notification for status of job in slack.

![image](https://user-images.githubusercontent.com/19822562/145600628-3e1fc2a4-7897-4400-b216-4e29dcb63f93.png)

![image](https://user-images.githubusercontent.com/19822562/145606974-31482253-c9cc-4af0-94ed-4f221250d172.png)

![image](https://user-images.githubusercontent.com/19822562/145607263-ee70daa2-8fe3-416f-b02c-9cfb43ad53c5.png)

